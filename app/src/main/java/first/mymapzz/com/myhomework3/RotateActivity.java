package first.mymapzz.com.myhomework3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class RotateActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rotate);
        ImageView imageView = findViewById(R.id.imageView);
        Animation animation = AnimationUtils.loadAnimation(this,R.anim.rotate);
        animation.setRepeatMode(Animation.REVERSE);
        animation.setRepeatCount(5);
        animation.setFillAfter(true);
        imageView.startAnimation(animation);
    }
}
