package first.mymapzz.com.myhomework3;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.opengl.Visibility;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class MainActivity extends AppCompatActivity {

    LinearLayout mLinearLayout;
    PopupWindow popupWindow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLinearLayout = findViewById(R.id.root);


    }

    private void hideActionBar() {

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
    }

    private void showActionBar() {

        ActionBar actionBar = getSupportActionBar();
        actionBar.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.options_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.add:
                Toast.makeText(MainActivity.this, "PopUp", Toast.LENGTH_SHORT).show();
                showPopupToAddData(new LinearLayout(this));
                return true;
            case R.id.fade_in:
                intentCustom(FadeInActivity.class);
                return true;
            case R.id.fade_out:
                intentCustom(FadeOutActivity.class);
                return true;
            case R.id.fade_zoom:
                intentCustom(ZoomActivity.class);
                return true;
            case R.id.rotate:
                intentCustom(RotateActivity.class);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }


    @RequiresApi(api = Build.VERSION_CODES.Q)
    private void showPopupToAddData(LinearLayout layout1) {


        hideActionBar();

        final EditText edTitle, edDescription;
        popupWindow = new PopupWindow(MainActivity.this);
        View layout = getLayoutInflater().inflate(R.layout.custom_popup, null);
        popupWindow.setContentView(layout);
        popupWindow.setAnimationStyle(R.style.Animation);
        popupWindow.showAtLocation(layout1, Gravity.CENTER, 0, 0);
        popupWindow.setFocusable(true);

        popupWindow.setTouchModal(false);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popupWindow.update();

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Button btnCancel = layout.findViewById(R.id.btn_cancel);
        edTitle = layout.findViewById(R.id.ed_title);
        edDescription = layout.findViewById(R.id.ed_desc);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Dismiss", Toast.LENGTH_SHORT).show();
                popupWindow.dismiss();
                showActionBar();
            }
        });
        Button btnSave = layout.findViewById(R.id.btn_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String title, desc;
                title = edTitle.getText().toString();
                desc = edDescription.getText().toString();
                if (isDataValid(title, desc)) {
                    createCardNote(title, desc);
                    Toast.makeText(MainActivity.this, "Note has been create", Toast.LENGTH_SHORT).show();
                    popupWindow.dismiss();
                    showActionBar();
                } else {
                    Toast.makeText(MainActivity.this, "Invalid", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    private void intentCustom(Class tempClass) {
        Intent intent = new Intent(MainActivity.this, tempClass);
        startActivity(intent);

    }

    private boolean isDataValid(String text1, String text2) {

        if (text1.isEmpty() || text1.length() == 0 || text2.isEmpty() || text2.length() == 0) {
            return false;
        }
        return true;
    }

    private void createCardNote(String title, String description) {

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
        CardView cardView = new CardView(MainActivity.this);
        cardView.setLayoutParams(layoutParams);
        cardView.setUseCompatPadding(true);
        cardView.setCardElevation(12);
        cardView.setContentPadding(16, 16, 16, 16);


        LinearLayout linearLayout = new LinearLayout(MainActivity.this);
        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);


        linearLayout.setPadding(25, 10, 25, 10);
        linearLayout.setBackgroundColor(Color.parseColor("#ffffff"));
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        //create text title
        TextView tvTitle = new TextView(MainActivity.this);
        tvTitle.setTypeface(Typeface.DEFAULT_BOLD);
        tvTitle.setMaxLines(1);
        tvTitle.setEllipsize(TextUtils.TruncateAt.END);
        tvTitle.setTextSize(16);
        tvTitle.setText(title);

        //create text desc
        TextView tvDesc = new TextView(MainActivity.this);
        tvDesc.setMaxLines(1);
        tvDesc.setEllipsize(TextUtils.TruncateAt.END);
        tvDesc.setText(description);

        linearLayout.addView(tvTitle, params1);
        linearLayout.addView(tvDesc, params2);
        cardView.addView(linearLayout);
        mLinearLayout.addView(cardView);
    }
}
